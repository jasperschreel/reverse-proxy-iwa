# How to set up Integrated Windows Authentication for a windows domain using a reverse proxy on IIS

## Setting up your AD
Install a DNS & DHCP server and cofigure them for your domain.
Set your main server as domain controller.
Then add users & computers to your domain.

## Install IIS
Through Server Manager -> Manage -> Add roles & features -> Install Web Server (IIS) but also drop down Web Server -> Application Development -> ISAPI Extensions and Filters

## IIS configuration
Open up your IIS Manager
Add Website
On the site open the feature Authentication
Enable Windows Authentication and disable the others

## Reverse proxy
Open feature URL Rewrite on your ISS site
Add Rule
* Rewrite URL http://website:port/{R:1}
* Add Server Variables
    * Name: X-Logon-User
    * Value: {LOGON_USER}

Check if inetpub/wwwroot/web.config file updated

![web.config](webconfig.png "web.config")

## Host a flask web app
Authentication will be done by Windows Authentication on the domain,
but we can still know what user is logged in thanks to our earlier setup Server Variable
<pre><code>request.headers.getlist("X-Logon-User")
</code></pre>

## Usefull links
[Configuring chrome and firefox for IWA](https://specopssoft.com/blog/configuring-chrome-and-firefox-for-windows-integrated-authentication/ "seamless authentication")