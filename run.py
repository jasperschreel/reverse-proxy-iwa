from flask import Flask
from flask import request
import json as json

app = Flask(__name__)

@app.route("/home", methods=["GET"])
def get_my_ip():
    return "hello"

@app.route("/")
def home():
    print(request.headers)
    if request.headers.getlist("X-Logon-User"):
        username = request.headers.getlist("X-Logon-User")[0].split("\\")[1]
        return "Hello " + username
    else:
        return "non IWA login from ip "+ request.remote_addr

    

if __name__ == "__main__":
    app.run(host= '0.0.0.0', port=8080)